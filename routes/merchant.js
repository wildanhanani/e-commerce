const express = require('express');
const merchantController = require('../controller/merchant');
const authMid = require('../midlleware/auth');
const scehmaMid = require('../midlleware/schema');

const router = express.Router();

router.post('/create', authMid.isUser, scehmaMid.midMercahnt, merchantController.createmerchant);

module.exports = router;
