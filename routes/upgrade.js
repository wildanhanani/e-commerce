const express = require('express');
const upgradeController = require('../controller/upgrade');
const authMid = require('../midlleware/auth');
const schemaMid = require('../midlleware/schema');

const router = express.Router();

router.post('/user/upgrade', authMid.isAdmin, schemaMid.midUpgradeUser, upgradeController.upgrade_user);

module.exports = router;
