const express = require('express');
const productController = require('../controller/product');
const authMid = require('../midlleware/auth');
const schemaMid = require('../midlleware/schema');

const router = express.Router();

router.post('/create', authMid.isUser, schemaMid.midProduct, productController.createproduct);

module.exports = router;
