const express = require('express');
const transactionController = require('../controller/transaction');
const authMid = require('../midlleware/auth');
const schemaMid = require('../midlleware/schema');

const router = express.Router();

router.post('/create', authMid.isUser, schemaMid.midTransaksi, transactionController.transaksi);
router.get('/view', authMid.isUser, transactionController.viewusertransaction);

module.exports = router;
