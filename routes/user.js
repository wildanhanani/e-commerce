const express = require('express');
const userController = require('../controller/user');
const schemaMid = require('../midlleware/schema');
const authMid = require('../midlleware/auth');

const router = express.Router();

router.post('/register/user', schemaMid.midRegister, userController.createuser);
router.post('/register/admin', schemaMid.midRegister, userController.createadmin);
router.post('/user/wallet/update', authMid.isUser, schemaMid.midUserWallet, userController.updateUserWallet);

module.exports = router;
