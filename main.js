const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const dotenv = require('dotenv');

const app = express();
const PORT = process.env.PORT || 4000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

if (process.env.NODE_ENV !== 'test') app.use(morgan('combined'));

dotenv.config();

const loginRoutes = require('./routes/login');
const merchantRoutes = require('./routes/merchant');
const productRoutes = require('./routes/product');
const transactionRoutes = require('./routes/transaction');
const upgradeRoutes = require('./routes/upgrade');
const userRoutes = require('./routes/user');

mongoose
  .connect(process.env.MONGODB_URI, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  })
  .then(() => console.log('mongodb connected'))
  .catch((err) => {
    console.log(err);
  });

app.get('/', (req, res) => {
  res.status(200).json({
    status: 200,
    message: 'e-commerce service up and running!',
    environment: process.env.NODE_ENV,
    timestamp: new Date(),
  });
});

const routerApiV1 = express.Router();

routerApiV1.use('/auth', loginRoutes);
routerApiV1.use('/merchant', merchantRoutes);
routerApiV1.use('/product', productRoutes);
routerApiV1.use('/transaction', transactionRoutes);
routerApiV1.use('/auth', upgradeRoutes);
routerApiV1.use('/auth', userRoutes);

app.use('/api/v1', routerApiV1);

app.use((req, res, next) => {
  const error = new Error('not found');
  error.status = 400;
  next(error);
});

// eslint-disable-next-line no-unused-vars
app.use((error, req, res, next) => {
  res.status(error.status || 500).json({
    status: error.status || 500,
    error: error.message,
  });
});

app.listen(PORT, console.log(`listening to PORT ${PORT}`));

module.exports = app;
